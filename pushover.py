import hexchat
import http.client, urllib

__module_name__ = 'pushover-away'
__module_version__ = '0.2.0'
__module_description__ = 'Sends an alert message when queried or named while away'

PUSHOVER_API = 'api.pushover.net:443'
PUSHOVER_APP_TOKEN = ''
PUSHOVER_USER_TOKEN = ''
# Notification Sound that you have configured at pushover.net
# Leave empty to use default sound configured in app
PUSHOVER_SOUND = ''

def send_pushover_message(message):
    try:
        conn = http.client.HTTPSConnection(PUSHOVER_API)
        conn.request("POST", "/1/messages.json",
                urllib.parse.urlencode({
                    "token": PUSHOVER_APP_TOKEN,
                    "user": PUSHOVER_USER_TOKEN,
                    "message": message,
                    "sound": PUSHOVER_SOUND
                 }),
                { "Content-type" : "application/x-www-form-urlencoded" })
        conn.getresponse()
    except:
        hexchat.prnt("Error with Pushover API")
        hexchat.prnt("Message: %s" % message)


def check_msg(word, word_eol, userdata, **kwargs):
    if hexchat.get_info('away') is not None:
        network = hexchat.get_info('network')
        channel = hexchat.get_info('channel')
        if channel == word[0]:
            channel = 'Private'
        message = '%s|%s - %s: %s' % (network, channel, word[0], word[1])
        send_pushover_message(message)


hexchat.hook_print("Channel Action Hilight", check_msg)
hexchat.hook_print("Channel Msg Hilight", check_msg)
hexchat.hook_print("Private Action to Dialog", check_msg)
hexchat.hook_print("Private Message to Dialog", check_msg)

hexchat.prnt('%(name)s, version %(version)s' % {'name': __module_name__,  'version': __module_version__})  
