# hexchat-pushover-away

This is a XChat/Hexchat plugin for sending [Pushover](https://pushover.net/) notifications on private messages or nick mentions while  you are set as Away.


##### PUSHOVER_APP_TOKEN
Your API token for the application configured in Pushover
  
##### PUSHOVER_USER_TOKEN
Your Pushover user key

##### PUSHOVER_SOUND
Your [Notification sound](https://pushover.net/api#sounds), either built-in or [custom](https://blog.pushover.net/posts/2021/3/custom-sounds), that you have configured at [pushover.net](https://pushover.net). 

Leave empty to use default sound configured in your Pushover app.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU1NjE2OTM3OF19
-->
